.DEFAULT_GOAL := help
RED=\033[0;31m
GREEN=\033[0;32m
ORANGE=\033[0;33m
BLUE=\033[0;34m
NC=\033[0m # No Color

.PHONY: install
install: ## Install the dependencies
	@echo "${GREEN}🤖 Installing dependencies${NC}"
	python3 -m pip install --upgrade pip
	python3 -m pip install --editable .

.PHONY: dev
dev: install ## Install the development dependencies
	python3 -m pip install --editable ".[dev]"

.PHONY: install-vendors
install-vendors: ## Install vendors dependencies
	@echo "${GREEN}🤖 Installing vendors dependencies${NC}"
	@python npm.py install

.PHONY: lint
lint: ## Ensure code consistency
	@echo "${GREEN}🤖 Linting code${NC}"
	@ruff check . --fix
	@ruff format . --quiet

passphrase = "letmein" # Is that really a good idea to provide a default?

.PHONY: build
build: ## Generate the site
	@mkdir -p public/static/
	@cp -R static public/
	@python3 generator.py build --passphrase=$(passphrase)

.PHONY: serve
serve: ## Launch a local server to serve the `public` folder.
	@python3 -m http.server 8000 --bind 127.0.0.1 --directory public

.PHONY: live
live: ## Rebuild contents on template change.
	@echo "${ORANGE}⚠️ You need http://eradman.com/entrproject/${NC}"
	ls templates/* | entr -r make build

.PHONY: help
help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

# See https://daniel.feldroy.com/posts/autodocumenting-makefiles
define PRINT_HELP_PYSCRIPT # start of Python section
import re, sys

output = []
# Loop through the lines in this file
for line in sys.stdin:
    # if the line has a command and a comment start with
    #   two pound signs, add it to the output
    match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
    if match:
        target, help = match.groups()
        output.append("\033[36m%-16s\033[0m %s" % (target, help))
# Sort the output in alphanumeric order
output.sort()
# Print the help result
print('\n'.join(output))
endef
export PRINT_HELP_PYSCRIPT # End of python section
