# PageCrypt web component

A Web Component to make a static page only readable with the associated password.

## Motivations

Inspired from [PageCrypt](https://github.com/MaxLaumeister/PageCrypt/) from
Maximillian Laumeister and the [updated version](https://github.com/Greenheart/pagecrypt) 
from Samuel Plumppu, I was motivated to create a dedicated Web Component.

The idea is to create an HTML page and the encapsulate the secret part with
`<page-crypt>` as you can see [with the demo template](templates/index.html).

You also have to initiate the Web Component itself:

```js
<script type="module">
  import {PageCrypt, register} from './static/pagecrypt-webcomponents.js'

  register(PageCrypt, 'page-crypt')
</script>
```

Once you have that in place, you can run the `build` command and it will 
generate a page in `./public/` with the encrypted part. Do not forget
to set your own `passphrase` parameter! (see below)

## Install

Pre-requisite: Python3.10+

Create and activate the virtualenv:

    $ python3 -m venv venv
    $ source venv/bin/activate

Install dependencies:

    $ make install

## Building

Launch command:

    $ make build passphrase=tralalalalère

## Running

Run local server:

    $ make serve

Go to http://127.0.0.1:8000/
