// Crypto stuff
const subtle = window.crypto?.subtle || window.crypto?.webkitSubtle

function str2ab(str) {
  const ustr = atob(str)
  const ustrLength = ustr.length
  const buf = new ArrayBuffer(ustrLength)
  const bufView = new Uint8Array(buf)
  for (let i = 0; i < ustrLength; i++) {
    bufView[i] = ustr.charCodeAt(i)
  }
  return bufView
}

async function deriveKey(salt, password, iterations) {
  const encoder = new TextEncoder()
  const baseKey = await subtle.importKey(
    'raw',
    encoder.encode(password),
    'PBKDF2',
    false,
    ['deriveKey']
  )
  return await subtle.deriveKey(
    { name: 'PBKDF2', salt, iterations, hash: 'SHA-256' },
    baseKey,
    { name: 'AES-GCM', length: 256 },
    true,
    ['decrypt']
  )
}

async function decryptFile({ salt, iv, ciphertext, iterations }, password) {
  const decoder = new TextDecoder()
  const key = await deriveKey(salt, password, iterations)
  const data = new Uint8Array(
    await subtle.decrypt({ name: 'AES-GCM', iv }, key, ciphertext)
  )
  return decoder.decode(data)
}

// Components
class PageCrypt extends HTMLElement {
  constructor() {
    super()

    if (!subtle) {
      console.error('Old browsers do not have the crypto.subtle lib.')
      return
    }

    const pagecryptContent = this.innerText
    if (!pagecryptContent) {
      console.error('No encrypted payload.')
      return
    }

    // Inject passphrase form in the component.
    this.innerHTML = pagecryptForm
    this.show(this)
    this.formElement = this.querySelector('form#page-crypt-form')
    this.passwordInput = this.formElement.querySelector('input[type="password"]')

    this.iterations = Number(this.dataset.iterations)
    const bytes = str2ab(pagecryptContent)
    this.salt = bytes.slice(0, 32)
    this.iv = bytes.slice(32, 32 + 16)
    this.ciphertext = bytes.slice(32 + 16)

    /**
     * Allow passwords to be automatically provided via the URI Fragment.
     * This greatly improves UX by clicking links instead of having to copy and paste the password manually.
     * It also does not compromise security since the URI Fragment is not sent across the internet.
     * Additionally, we delete the URI Fragment from the browser address field when the page is loaded.
     *
     * NOTE: However, beware that the password remains as a history entry if you use magic links!
     * Feel free to submit a PR if you know a workaround for this.
     */
    if (location.hash) {
      const parts = location.href.split('#')
      this.passwordInput.value = parts[1]
      history.replaceState(null, '', parts[0])
    }

    this.handleDecryption()
  }

  async handleDecryption() {
    if (this.passwordInput.value) {
      await this.decrypt()
    } else {
      this.show(this.formElement)
      this.passwordInput.focus()
    }

    this.formElement.addEventListener('submit', async (event) => {
      event.preventDefault()
      await this.decrypt()
    })
  }

  async decrypt() {
    this.hide(this.formElement)
    await this.sleep(this.dataset.throttle)

    try {
      const decrypted = await decryptFile(
        {
          salt: this.salt,
          iv: this.iv,
          ciphertext: this.ciphertext,
          iterations: this.iterations,
        },
        this.passwordInput.value
      )
      this.innerHTML = decrypted
      this.show(this)
    } catch (e) {
      console.error(e)
      this.show(this.formElement)

      this.passwordInput.value = ''
      this.passwordInput.focus()
      this.passwordInput.setAttribute('aria-invalid', 'true')
      this.passwordInput.setAttribute('aria-describedby', 'invalid-helper')
      this.show(this.formElement.querySelector('#invalid-helper'))
    }
  }

  // Utils
  show(element) {
    element.removeAttribute('hidden')
  }

  hide(element) {
    element.setAttribute('hidden', 'hidden')
  }

  async sleep(milliseconds) {
    return new Promise((resolve) => setTimeout(resolve, milliseconds))
  }
}

const pagecryptForm = `
  <form id="page-crypt-form">
    <small>The password for this demo is <code>letmein</code></small>
    <fieldset role="group">
      <input
        id="password"
        type="password"
        name="password"
        placeholder="Enter your password"
        autocomplete="new-password"
        aria-label="Password"
      />
      <label for="password" class="sr-only">Password:</label>
      <input type="submit" value="Decrypt the page" />
    </fieldset>
    <small id="invalid-helper" hidden>Please provide the correct password</small>
  </form>
`

function register(Class, tagName) {
  if ('customElements' in window && !customElements.get(tagName)) {
    customElements.define(tagName, Class)
  }
}

export { PageCrypt, register }
