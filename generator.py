from base64 import b64encode
from pathlib import Path

import minicli
from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto.Protocol.KDF import PBKDF2
from selectolax.parser import HTMLParser

__version__ = "0.0.1"

HERE = Path().resolve()
TEMPLATES = HERE / "templates"
PUBLIC = HERE / "public"


def encrypt(content, passphrase, iterations):
    # Crypto part adapted from:
    # https://github.com/MaxLaumeister/PageCrypt/blob/master/python/encrypt.py
    # kudos to Zoltán Gálli <https://zoltan.xyz/>
    salt = Random.new().read(32)
    key = PBKDF2(
        passphrase.encode(),
        salt,
        count=iterations,
        dkLen=32,
        hmac_hash_module=SHA256,
    )
    iv = Random.new().read(16)

    cipher = AES.new(key, AES.MODE_GCM, nonce=iv)
    encrypted, tag = cipher.encrypt_and_digest(content.encode())

    return b64encode(salt + iv + encrypted + tag).decode()


@minicli.cli
def build(passphrase="letmein", iterations=2_000_000, throttle=60):
    template_content = (TEMPLATES / "index.html").read_text()

    tree = HTMLParser(template_content)
    pagecrypt_node = tree.css_first("page-crypt")
    pagecrypt_node.attrs["data-iterations"] = iterations
    pagecrypt_node.attrs["data-throttle"] = throttle
    pagecrypt_node.attrs["hidden"] = "hidden"
    pagecrypt_div = pagecrypt_node.css_first("div")
    pagecrypt_content = pagecrypt_div.html
    encrypted_payload = encrypt(pagecrypt_content, passphrase, iterations)
    pagecrypt_div.replace_with(encrypted_payload)
    template_content = tree.html

    Path(PUBLIC / "index.html").write_text(template_content)


if __name__ == "__main__":
    minicli.run()
